<a style="text-align: center" href="https://github.com/TryGhost/Ghost"><img src="https://cloud.githubusercontent.com/assets/120485/18661790/cf942eda-7f17-11e6-9eb6-9c65bfc2abd8.png" alt="Ghost" /></a>
<br/>
# Status
Project is on hold while ghost-src finishes the authentication logic for their API. The app store is not a place for beta back-ends :).

Update: It looks like Ghost has a v2 page on their API documentation, however it doesn't have any actualy documentation in it as of 28 Dec 2018. I'll keep checking in periodically. Looking forward to being able to complete this.

Update 25 Jan 2019: Still waiting on the new API from Ghost-Src :(

# Copyright & License

Copyright (c) 2013-2018 Ghost Foundation - Released under the [MIT license](LICENSE). Ghost and the Ghost Logo are trademarks of Ghost Foundation Ltd. Please see our [trademark policy](https://ghost.org/trademark/) for info on acceptable usage.
