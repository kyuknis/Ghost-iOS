//
//  UserServiceProvider.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/21/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation
import Alamofire

class UserServiceProvider {
    private var _instance: UserServiceProvider?
    
    public var instance: UserServiceProvider {
        get {
            if self._instance == nil {
                self._instance = UserServiceProvider()
            }
            
            return self._instance!
        }
    }
    
    private init() {
        //
    }
    
    public func fetchAll(until numberPosts: Int = 15) -> [Post] {
        return [Post]()
    }
    
    public func fetchUser(postWithId postId: Int) -> Post {
        return Post()
    }
    
    public func createUser(withPost post: Post) -> Bool {
        return true
    }
}
