//
//  PostServiceProvider.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/21/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation
import Alamofire

class PostServiceProvider {
    
    private static var _instance: PostServiceProvider?
    private var _previousRequestsMeta: Meta = Meta()
    public var posts: [Post] = []
    
    public static var instance: PostServiceProvider {
        get {
            if self._instance == nil {
                self._instance = PostServiceProvider()
            }
            
            return self._instance!
        }
    }
    
    public var previousRequestsMeta: Meta {
        get {
            return self._previousRequestsMeta
        }
        set {
            self._previousRequestsMeta = newValue
        }
    }
    
    private init() {
        //
    }
    
    public func fetchNext(_ completion: @escaping CompletionHandler) -> Void {
        
//        let uri: String = self.previousRequestsMeta == nil ? pathForResource(resource: Post.self) : self.previousRequestsMeta.pagination.next
        let uri: String = pathForResource(resource: Post.self)
        
        Alamofire.request(uri).responseJSON { response in
            if let json = response.result.value as? Dictionary<String, Any> {
                if response.result.error == nil {
                    self.fetchNext_didSucceed(withResponse: json)
                    completion(true)
                } else {
                    completion(true)
                }
            }
        }
        
    }
    
    public func fetchNext_didSucceed(withResponse response: Dictionary<String, Any>) -> Void {
        
        // Parse and add all of the posts to the posts array
        if let jsonPosts = response["posts"] as? Array<Dictionary<String, Any>> {
            for jsonPost in jsonPosts {
                self.posts.append(Post(fromDictionary: jsonPost))
            }
        }
        
        // Update the providers meta for the next request
        if let jsonMeta = response["meta"] as? Array<Dictionary<String, Any>> {
            self.previousRequestsMeta = Meta()
        }
    }
    
    public func fetchPost(postWithId postId: Int) -> Post {
        return Post()
    }
    
    public func savePost(withPost post: Post) -> Bool {
        return true
    }
}
