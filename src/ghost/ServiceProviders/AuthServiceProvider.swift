//
//  AuthServiceProvider.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/21/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation
import Alamofire

class AuthServiceProvider {
    private var _instance: AuthServiceProvider?
    
    public var instance: AuthServiceProvider {
        get {
            if self._instance == nil {
                self._instance = AuthServiceProvider()
            }
            
            return self._instance!
        }
    }
    
    private init() {
        //
    }
    
}
