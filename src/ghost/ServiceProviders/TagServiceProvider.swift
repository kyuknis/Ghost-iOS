//
//  TagServiceProvider.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/21/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation
import Alamofire

class TagServiceProvider {
    private var _instance: TagServiceProvider?
    
    public var instance: TagServiceProvider {
        get {
            if self._instance == nil {
                self._instance = TagServiceProvider()
            }
            
            return self._instance!
        }
    }
    
    private init() {
        //
    }
    
    public func fetchAll(until numberPosts: Int = 15) -> [Post] {
        return [Post]()
    }
    
    public func fetchTag(postWithId postId: Int) -> Post {
        return Post()
    }
    
    public func saveTag(withPost post: Post) -> Bool {
        return true
    }
}
