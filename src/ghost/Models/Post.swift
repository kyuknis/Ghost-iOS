//
//  Post.swift
//  Ghost
//
//  Created by Ray Bradley on 4/24/18.
//  Copyright © 2018 Analemma Heavy Industries. All rights reserved.
//
//  Typical REST call
//  https://theojisan.com/ghost/api/v0.1/posts/?limit=30&page=1&status=all&staticPages=all&formats=mobiledoc,plaintext&include=tags

import Foundation

enum PostStatus : String {
    case Draft = "draft", Published = "published", Scheduled = "scheduled"
}

class Post: GhostModel {
    
    var _id: String = ""
    var _title: String = ""
    var _author: String = ""
    var _status: String = ""
    var _excerpt: String = ""
    var _markdown: String?
    var _tags: [Tag] = []
    var _updatedAt: Date = Date()
    var _createdAt: Date = Date()
    var _publishedAt: Date?
    
    var id: String {
        get {
            return self._id
        }
        set {
            self._id = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var title: String {
        get {
            return self._title
        }
        set {
            self._title = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var author: String {
        get {
            return self._author
        }
        set {
            self._author = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var status: String {
        get {
            return self._status
        }
        set {
            self._status = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var excerpt: String {
        get {
            return self._excerpt
        }
        set {
            self._excerpt = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var markdown: String {
        get {
            return self._markdown!
        }
        set {
            self._markdown = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var tags: [Tag] {
        get {
            return self._tags
        }
        set {
            self._tags = newValue
        }
    }
    
    var updatedAt: Date {
        get {
            return self._updatedAt
        }
        set {
            self._updatedAt = newValue
        }
    }
    
    var createdAt: Date {
        get {
            return self._createdAt
        }
        set {
            self._createdAt = newValue
        }
    }
    
    var publishedAt: Date {
        get {
            return self._publishedAt!
        }
        set {
            self._publishedAt = newValue
        }
    }
    
    var new: Bool! {
        get {
            return _id == ""
        }
    }
    
    var isPublished: Bool! {
        get {
            return self._status.lowercased() == PostStatus.Published.rawValue
        }
    }
    
    
    var isDraft: Bool! {
        get {
            return self._status.lowercased() == PostStatus.Draft.rawValue
        }
    }
    
    var isScheduled: Bool! {
        get {
            return self._status.lowercased() == PostStatus.Scheduled.rawValue
        }
    }
    
    override init() {
        //
    }
    
    convenience init(fromDictionary dictionary: Dictionary<String, Any>) {
        self.init()
        
        if let s_title = dictionary["title"] as? String {
            self.title = s_title
        }
        
    }
    
    //
    // liberal inspiration from https://github.com/TryGhost/Ghost-Android/blob/8f31cefbf3caed339cf00726872d131eb2ddefa2/app/src/main/java/me/vickychijwani/spectre/network/GhostApiUtils.java
    //
    
    //
    // TODO: move this out to a new class (MobileDocSerializer?)
    //
    var mobiledoc: String? {
        get {
            let escapedMarkdown = markdown
                .replacingOccurrences(of: "\\", with: "\\\\")
                .replacingOccurrences(of: "\"", with: "\\\"")
                .replacingOccurrences(of: "\n", with: "\\n")
            //        .replacingOccurrences(of: "\t", with: "\\\t")
            //        .replacingOccurrences(of: "\r", with: "\\\r")
            
            return "{" +
                "  \"version\": \"0.3.1\"," +
                "  \"markups\": []," +
                "  \"atoms\": []," +
                "  \"cards\": [" +
                "    [\"card-markdown\", {" +
                "      \"cardName\": \"card-markdown\"," +
                "      \"markdown\": \"\(escapedMarkdown)\"" +
                "    }]" +
                "  ]," +
                "  \"sections\": [[10, 0]]" +
            "}"
        }
    }
    
}
