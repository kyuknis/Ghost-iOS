//
//  Pagination.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/24/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation

class Pagination {
    private var _next: String?
    
    public var next: String {
        get {
            return self._next!
        }
        set {
            self._next = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
}
