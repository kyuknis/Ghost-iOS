//
//  Meta.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/24/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation

class Meta {
    private var _pagination: Pagination?
    
    public var pagination: Pagination {
        get {
            return self._pagination!
        }
        set {
            self._pagination = newValue
        }
    }
    
    init() {
        //
    }
}
