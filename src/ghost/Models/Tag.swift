//
//  Tag.swift
//  Ghost
//
//  Created by Ray Bradley on 4/30/18.
//  Copyright © 2018 Analemma Heavy Industries. All rights reserved.
//

import Foundation

class Tag: GhostModel {
    private var _id:   String = ""
    private var _name: String = ""
    
    public var id: String {
        get {
            return self._id
        }
        set {
            self._id = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    public var name: String {
        get {
            return self._name;
        }
        set {
            self._name = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    override init() {
        //
    }
    
    init(fromJson json: NSDictionary) {
        //
    }
}
