//
//  Tag.swift
//  Ghost
//
//  Created by Ray Bradley on 4/30/18.
//  Copyright © 2018 Analemma Heavy Industries. All rights reserved.
//

import Foundation

class User: GhostModel {
    
    private var _id: String?
    private var _name: String?
    private var _slug: String?
    private var _profileImage: String?
    private var _coverImage: String?
    private var _bio: String?
    private var _website: String?
    private var _location: String?
    private var _facebook: String?
    private var _twitter: String?
    private var _accessibility: String?
    private var _locale: String?
    private var _visibility: String?
    private var _metaTitle: String?
    private var _metaDescription: String?
    private var _tour: String?
    
    public var id: String {
        get {
            return self._id!
        }
        set {
            self._id = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var name: String {
        get {
            return self._name!
        }
        set {
            self._name = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    override init() {
        //
    }
    
    init(fromJson json: NSDictionary) {
        
    }
}
