//
//  ConfigHelpers.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/24/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation

var GHOST_API_BASE: String {
    get {
        let CLEAN_BASE: String = GHOST_API_BASE_URL.trimmingCharacters(in: CharacterSet.init(charactersIn: "/"))
        return "\(CLEAN_BASE)/\(GHOST_API_BASE_VERSION)"
    }
}

func pathForResource(resource resourceType: Any) -> String {
    let resourceName: String = "\(String(describing: resourceType).lowercased())s"
    return "\(GHOST_API_BASE)/\(resourceName)?client_id=\(CLIENT_ID)&client_secret=\(CLIENT_SECRET)"
}
