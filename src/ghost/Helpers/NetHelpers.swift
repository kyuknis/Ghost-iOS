//
//  NetHelpers.swift
//  ghost
//
//  Created by Kirkland Yuknis on 7/25/18.
//  Copyright © 2018 Yuknis. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()
